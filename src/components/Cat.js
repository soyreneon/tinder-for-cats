import "./Cat.css";
const Cat = ({ name, img, desc, hobbies, index, setCount }) => {
    const onClick = () => {
        let i = index
        setCount(++i)
    }
    return (
        <section className="catcard">
            <header className="catbox">
                <div className="catimg"
                    style={{ backgroundImage: `url(${img})` }}></div>
                <div className="container-btn">
                    <button className="yes-btn" onClick={onClick}>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                        </svg>
                    </button>
                    <button className="no-btn" onClick={onClick}>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </header>
            <footer className="catbox margin-10 cat-info">
                <div className="padding info">
                    <h2>{name}</h2>
                    <p>{desc}</p>
                    <h3>Hobbies</h3>
                    <div>
                        {hobbies && hobbies.map((hobbie) => (
                            <i className='hobbie'>{hobbie}</i>
                        ))}
                    </div>
                </div>
            </footer>
        </section>
    );
};

export default Cat;
